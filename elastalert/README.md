# ElastAlert Docker Image

## Abstract

Se dispone de los ficheros necesarios para poder ejecutar un _build_ de la imagen para _ElastAlert_.
Probada sobre _Openshift Container Platform 3.9_ y conectándose al _stack EFK_.

## Construcción de imagen y despliegue

Tanto los pasos para la construcción de la imagen como el despliegue tienen su propia documentación [AQUÍ](../Documentation/ElastAlert/)

## Breve explicación de variables incluidas en las reglas y en la configuración

**configmap: sl-elastalert-cm**<br>
Incluye las configuraciones necesarias para ElastAlert, dividido en dos claves que son montadas en el contenedor para su lectura como fichero _YAML_

**Clave: config.yaml**<br>
Fichero de configuración para ElastAlert.

_rules_folder: rules_<br>
Carpeta donde se alojan los diferentes ficheros _YAML_ con las reglas configuradas y que son notificadas

_scan_subdirectories: false_<br>
Para habilitar si entro del directorio de reglas se van a recorrer niveles más profundos de directorios, al incluir las reglas como _configmap_ e ir a la raiz de su punto de montaje, no es necesario

_run_every:_<br>
  _minutes: 1_<br>
Cada cuanto tiempo se evaluan las reglas<br>
Soporta __days,hours, minutes, seconds__

_buffer_time:_<br>
  _minutes: 15_<br>
Tiempo sobre el cual ElastAlert toma si una alerta ya ha sido vista en una ejecución anterior para no volver a "dispararla"

_es_host: logging-es_<br>
La ruta hasta el servidor de _ElasticSearch_

_es_port: 9200_<br>
Puerto donde conectar con _ElasticSearch_

_writeback_index: elastalert_status_<br>
Índice de _ElasticSearch_ donde ElastAlert anotará la búsqueda de las alertas que son disparadas y si han sido notificadas o no.<br>
Si no existe en el primer arranque lo crea

_alert_time_limit:_<br>
  _days: 2_<br>
Si una alerta falla al ser notificada, durante cuanto tiempo será reintentada la notificación.<br>
Soporta __days,hours, minutes, seconds__

_use_ssl: True_.<br>
_verify_certs: True_.<br>
Activar o desactivar el uso de conexión SSL y la verificación de los certificados

_ca_certs: /etc/curator/keys/admin-ca_.<br>
_client_cert: /etc/curator/keys/admin-cert_.<br>
_client_key: /etc/curator/keys/admin-key_.<br>
Certificados usador por _Curator_ para su conexión con _ElasticSearch_, son los mismos empleados por ElastAlert..<br>
En la peculiaridad de OCP, son necesarios los certificados de administrador para poder escribir sobre los índices

_smtp_host: "smtpapl.sls.inf"_.<br>
Servidor SMTP.<br>
En el caso de la plataforma de Santalucía se emplea un relay

_smtp_ssl: true_.<br>
Habilitación o no del uso de ssl contra el servidor de correo..<br>
En el caso de la plataforma de Santalucía no es necesario

_smtp_port: 25_.<br>
Puerto donde conectar con el servidor de correo

_from_addr: "elastalert@santalucia.es"_.<br>
Dirección de origen que envía los emails de notificación

_email:_.<br>
_- "sistemas@santalucia.es"_.<br>
Lista de emails que recibirán los correos de alertas..<br>
Los emails incluidos aquí son notificados siempre que una regla en concreto no modifique este valor

_smtp_auth_file: "/opt/elastalert/config/smtp_auth.yaml"_.<br>
Fichero que aloja las credenciales de autenticación edl servidor de correo, si no es necesario debe ser eliminado/comentado.

**Clave: smtp_auth.yaml**
Fichero con las credenciales del servidor SMTP
En el entorno de Santalucía no es necesario ya que existe un relay sin autenticar

## Breve explicación para agregar/retirar una alerta

Agregar una nueva regla es tan sencillo como incluir un nuevo par de clave/valor al configmap, siguiendo el estándar de los _YAML_ de kubernetes/_OCP_

>  NOMBRE_DE_LA_CLAVE(ALERTA).yaml: |-
>    Valor

Para retirar una alerta de ElastAlert basta con eliminar el contenido completo del par clave/valor que la incluye ó si por algún motivo que quiera volver a emplear en el futuro, también es suficiente con dejarla comentada en el _configmap_

## Breve explicación de las variables usadas en las alertas

Documentación completa con todas las variables posibles: [AQUÍ](https://elastalert.readthedocs.io/en/latest/ruletypes.html#)

Variables usadas en las alertas configuradas actualmente

_name: RULE_NAME_<br>
Nombre de la alerta, debe ser único

_type: frequency_<br>
Tipo de alerta, el tipo __frequency__ indica que la alerta es disparada al ocurrir el evento un determinado número de veces en el tiempo indicado

_index: .all*_<br>
Índice de _ElasticSearch_ donde realizar la búsqueda para comprobar si el evento a ocurrido

_num_events: 1_<br>
Número de veces que tiene que ocurrir el evento en el tiempo indicado para que la alerta sea "disparada"

_timeframe:_<br>
_>minutes: 15_<br>
Rango de tiempo para comprobar la cantidad de eventos ocurridos para decidir si se toma la alerta como "disparada"

_filter:_<br>
_- query_string:_<br>
_query: 'CIDRAssignmentFailed'_<br>
Filtros/Queries a ejecutar sobre _ElasticSearch_ que son tomados como el evento ocurrido

_alert:_<br>
_- "email"_<br>
Habilitar la alerta al detectar el evento ocurrido en el número de veces indicadas en el tiempo específicado.<br>
También se indica la forma de notificación para alerta "disparada" en este caso vía email.

## Breve explicación para enviar alertas a otros destinos direferentes al correo genérico

Si se desea que una alerta en concreto no envíe la notificación al email genérico configurado a nivel global de ElastAlert, se pueden agregar una lista de emails de destino que sobreescribe dicha configuración para una alerta en concreto.

El el par de clave/valor de la regla deseada, después del apartado que indica que la notificación es enviada vía email<br>

>The alert is use when a match is found (Required)
>alert:
>- "email"'

Agregando la lista de emails de la siguiente forma

>email:
>- "email1@destino1.com"
>- "email2@destino2.com"

__NOTA:__ Se recuerda que indicar destinatarios en una regla específica sobreescribe el envio al email configurado a nivel global, por lo que si se desea que a mayores del email genérico sea otra cuenta la que reciba la notificación, ambos destinatarios deberán ser incluidos en la lista de emails

## Notas importantes

Inicialmente, y partiendo de la documentación oficial del proyecto _ElastAlert_ se emplean los certificados que dispone _curator_. Para el caso específico de _OCP_, estos certificados parecen ser de "solo lectura" por lo que no es posible realizar un "purgado" de las alertas ya notificadas y anotadas en el índice generado (por defecto *elastalert_status*).

Para resolver esta _issue_ se emplean los certificados de administrador del propio _ElasticSearch_, que son generados durante el despliege del mismo y bajo el _secret_ -> _logging-elasticsearch_
